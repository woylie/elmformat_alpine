FROM node:8.15.0-alpine

RUN apk add bash
RUN apk add yarn

RUN yarn global add elm-format

WORKDIR /app
