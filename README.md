# Elm-format on Alpine

Build image and open container bash:

```bash
docker build . -t elmformat_alpine
docker run -it -v ${PWD}:/app elmformat_alpine bash
```

Then execute:

```bash
elm-format src/
```

This will result in:

```
events.js:183
      throw er; // Unhandled 'error' event
      ^

Error: spawn /usr/local/share/.config/yarn/global/node_modules/elm-format/unpacked_bin/elm-format ENOENT
    at Process.ChildProcess._handle.onexit (internal/child_process.js:190:19)
    at onErrorNT (internal/child_process.js:362:16)
    at _combinedTickCallback (internal/process/next_tick.js:139:11)
    at process._tickCallback (internal/process/next_tick.js:181:9)
    at Function.Module.runMain (module.js:696:11)
    at startup (bootstrap_node.js:204:16)
    at bootstrap_node.js:625:3
```
